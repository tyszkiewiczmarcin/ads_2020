
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define CHUNK_LEN 16
#define CHUNK_MAXVALUE 10000000000000000

typedef struct node {
    char c;
    struct node* next;
} list_t;

typedef struct {
    long long* chunk;
    int size;
    bool positive;
} bigint_t;

typedef struct {
    long long i, j, k;
    char sign;
} param_t;

long long power(int x, int y) {
    if (y==0) return 1;
    long long result = 1;
    for (int i=1; i<=y; i++)
        result *=x;
    return result;
}

int chunk_num(int length) {
    int result = 0;
    for (int i=0; i<=length; i++)
        if (i % CHUNK_LEN == 1)
            result++;
    return result;
}

void insert(list_t** head, char number_character) {
    list_t* new_node = (list_t*)malloc(sizeof(list_t));
    new_node->c = number_character;
    new_node->next = *head;
    *head = new_node;
}

void freeList(struct node* head) {
   struct node* tmp;
   while (head != NULL) {
       tmp = head;
       head = head->next;
       free(tmp);
    }

}

void print_num(bigint_t* X) {
    if (!X->positive) printf("-");
    for (int i = 0; i < X->size; i++) {
        if (i != 0) {
            for (long long x = CHUNK_MAXVALUE/10; x>10; x /= 10)
                if (X->chunk[i] < x) printf("0");
        }
        printf("%lld",(X->chunk)[i]);
    }
    printf("\n");
}

void print_all(bigint_t** tab, int n) {
    for (int i=0; i<n; i++)
        print_num(tab[i]);
}

bool bigger(bigint_t* A, bigint_t* B) {
    if (A->size > B->size)
        return true;
    if (A->size < B->size)
        return false;
    if (A->size == B->size)
        for (int i=0; i<A->size;i++) {
            if (A->chunk[i] > B->chunk[i])
                return true;
            if (A->chunk[i] < B->chunk[i])
                return false;
            if (A->chunk[i] == B->chunk[i])
                continue;
        }
    return false;
}

void max(bigint_t** arr, int n) {
    bigint_t* biggest = arr[0];
    for (int i=0; i<n-1; i++)
    {
        if (biggest->positive == false) {
            if (arr[i+1]->positive == true)
                biggest = arr[i+1];
            if (biggest->size > arr[i+1]->size)
                biggest = arr[i+1];
            if (biggest->size == arr[i+1]->size)
                if (biggest->chunk[0] > arr[i+1]->chunk[0])
                    biggest = arr[i+1];
        }
        if (biggest->positive == true) {
            if (arr[i+1]->positive ==false)
                continue;
            if (biggest->size < arr[i+1]->size)
                biggest = arr[i+1];
            if (biggest->size == arr[i+1]->size)
                if (biggest->chunk[0] < arr[i+1]->chunk[0])
                    biggest = arr[i+1];
        }
    }
    print_num(biggest);
}

void min(bigint_t** arr, int n) {
    bigint_t* smallest = arr[0];
    for (int i=0; i<n-1; i++)
    {
        if (smallest->positive == false) {
            if (arr[i+1]->positive == true)
                continue;
            if (smallest->size < arr[i+1]->size)
                smallest = arr[i+1];
            if (smallest->size == arr[i+1]->size)
                if (smallest->chunk[0] < arr[i+1]->chunk[0])
                    smallest = arr[i+1];
        }
        if (smallest->positive == true) {
            if (arr[i+1]->positive == false)
                smallest = arr[i+1];
            if (smallest->size > arr[i+1]->size)
                smallest = arr[i+1];
            if (smallest->size == arr[i+1]->size)
                if (smallest->chunk[0] > arr[i+1]->chunk[0])
                    smallest = arr[i+1];
        }
    }
    print_num(smallest);
}

bigint_t* bigint_create() {
    char char_digit;
    bigint_t* bigint = (bigint_t*)malloc(sizeof(bigint_t));
    bigint->positive = 1;
    int length = 0;
    
    list_t* input_num = NULL;
    while (scanf("%c",&char_digit)) {
        if (char_digit == '\n') {
            if (length != 0)
                break;
            else
                scanf("%c",&char_digit);
        }
        if (char_digit=='-')
            bigint->positive = 0;
        else if (((int)char_digit-48) >= 0 && ((int)char_digit-48) <= 9) {
            insert(&input_num, char_digit);
            length++;
        }
        //character = getchar();
    }
    bigint->size = chunk_num(length);
    //printf("chunk num= %d\n",chunk_num(length));
    
    long long* CH = (long long*)malloc((bigint->size) * sizeof(long long));
    int chunk_id = bigint->size - 1;
    for (int i=chunk_id; i>=0; i--) CH[i]=0;
    for (int digit = 0; digit < length; digit++) {
        if (digit % CHUNK_LEN == 0 && digit != 0) {
            chunk_id--;
            CH[chunk_id] = 0;
        }
        CH[chunk_id] += ((int)input_num->c - 48) * power(10, digit % CHUNK_LEN);
        input_num = input_num->next;
        //printf("%d.\tCH[%d]=%llu\n",digit,chunk_id,CH[chunk_id]);
    }
    freeList(input_num);
    bigint->chunk = CH;
    
    //printf("created..\n");
    return bigint;
}

bigint_t** create_bigint_table(int n) {
    bigint_t** table = (bigint_t**)malloc(n * sizeof(bigint_t));
    for (int i=0; i<n; i++) {
        table[i] = bigint_create();
        //printf("Created %d / %d\n",i,n);
    }
    return table;
}

bigint_t* add(bigint_t* A, bigint_t* B) {
    int extra_chunk = false;
    // START READING CHUNKS FROM THE LAST ONE
    int tail_index_A = A->size - 1;
    int tail_index_B = B->size - 1;
    int C_size;
    (tail_index_A > tail_index_B) ? (C_size = A->size) : (C_size = B->size);
    
    long long* C = (long long*)malloc(C_size * sizeof(long long));
    for (int i=0; i < C_size; i++)
        C[i] = 0;
    
    bigint_t* result = (bigint_t*)malloc(sizeof(bigint_t));
    (A->positive) ? (result->positive=true) : (result->positive=false);
    result->size = C_size;
    
    
    for (int i = C_size - 1; i >= 0; i--) {
        if (tail_index_A >= 0) C[i] += A->chunk[tail_index_A];
        if (tail_index_B >= 0) C[i] += B->chunk[tail_index_B];
        if (C[i] >= CHUNK_MAXVALUE) {
            C[i] -= CHUNK_MAXVALUE;
            if (i == 0)
                extra_chunk = true;
            else
                C[i - 1] += 1;
        }
        tail_index_A -= 1;
        tail_index_B -= 1;
    }
    
    result->chunk = C;
    
    if (extra_chunk) {
        result->size = C_size + 1;
        long long* RESULT_NEW = (long long*)malloc((C_size + 1) * sizeof(long long));
        RESULT_NEW[0] = 1;
        for (int i = 1; i <= result->size; i++)
            RESULT_NEW[i] = C[i-1];
        result->chunk = RESULT_NEW;
        //free(C_new);
    }
    
    return result;
}

bigint_t* substr(bigint_t* A, bigint_t* B) {
    int size = A->size;
    int first_B = B->size - A->size;
    
    long long* C = (long long*)malloc(size * sizeof(long long));
    for (int i = 0; i < size; i++)
        C[i] = 0;
    
    for (int first_C = 0; first_C < size; first_C++, first_B++) {
        if (first_B < 0) {
            C[first_C] += A->chunk[first_C];
            continue;
        }
        else
            C[first_C] = A->chunk[first_C] - B->chunk[first_B];
        
        if (C[first_C] < 0) {
            C[first_C] += CHUNK_MAXVALUE;
            C[first_C-1] -= 1;
        }
    }
    
    bigint_t* result = (bigint_t*)malloc(sizeof(bigint_t));
    result->size = size;
    int size_sub = 0;
    while (C[size_sub] == 0) {
        result->size--;
        size_sub++;
    }
    
    // change size
    if (result->size != size) {
        long long* C_new = (long long*)malloc(result->size * sizeof(long long));
        int diff = size - result->size;
        for (int i = 0; i < result->size; i++)
            C_new[i] = C[i + diff];
        result->chunk = C_new;
        free(C);
        }
    
    else result->chunk = C;
    //print_num(result);
    
    return result;
}

void evaluate(bigint_t** arr, param_t* EQ) {
    bigint_t* A = arr[EQ->j];
    bigint_t* B = arr[EQ->k];
    bigint_t* C = A;
    if (A->positive) {
        if (B->positive) {
            
            if (EQ->sign=='+')                                       // (+) + (+) = (+)
                C = add(A, B);
            else if (EQ->sign=='-') {                                // (+) - (+) = (?)
                if (bigger(A, B)) {
                    C = substr(A, B);
                    C->positive = true;
                }
                else {
                    C = substr(B, A);
                    C->positive = false;
                }
            }
        }
        else {
            
            if (EQ->sign=='+') {                                     // (+) + (-) = (?)
                if (bigger(A, B)) {
                    C = substr(A, B);
                    C->positive = true;
                }
                else {
                    C = substr(B, A);
                    C->positive = false;
                }
            }
            else if (EQ->sign=='-')                                  // (+) - (-) = (+)
                C = add(A, B);
        }
    }
    
    else {
        
        if (B->positive) {
            if (EQ->sign=='+') {                                     // (-) + (+) = (?)
                if (bigger(A, B)) {
                    C = substr(A, B);
                    C->positive = false;
                }
                else {
                    C = substr(B, A);
                    C->positive = true;
                }
            }
            else if (EQ->sign=='-')                                  // (-) - (+) = (-)
                C = add(A, B);
        }
        
        else {
            if (EQ->sign=='+')                                       // (-) + (-) = (-)
                C = add(A, B);
            else  if (EQ->sign=='-') {                               // (-) - (-) = (?)
                if (bigger(A, B)) {
                    C = substr(A, B);
                    C->positive = false;
                }
                else {
                    C = substr(B, A);
                    C->positive = true;
                }
            }
        }
    }
    arr[EQ->i] = C;
}

void read_command(bigint_t** T, int n) {
    param_t* param = (param_t*)malloc(sizeof(param_t));
    char* CMD = (char*)malloc(30*sizeof(char));
    bool exit = false;
    char character;
    int cmd_id = 0;
    
    while (!exit) {
        while (scanf("%c",&character)) {
            CMD[cmd_id] = character;
            if (character == '\n' || character == '\0') {
                cmd_id = 0;
                if (sscanf(CMD, "%lld = %lld - %lld", &param->i, &param->j, &param->k) == 3) {
                    param->sign = '-';
                    evaluate(T, param);
                }
                else if (sscanf(CMD, "%lld = %lld + %lld", &param->i, &param->j, &param->k) == 3) {
                    param->sign = '+';
                    evaluate(T, param);
                }
                for (int i=0; i<30; i++) CMD[i] = 0;
                break;
            }
            cmd_id++;
            if (character == 'm') {
                scanf("%c",&character);
                scanf("%c",&character);
                if (character == 'n') min(T, n);
                if (character == 'x') max(T, n);
            }
            if (character == '?') print_all(T, n);
            if (character == 'q') exit = true;
        }
    }
    free(CMD);
}

int main() {
    int n;
    //printf("n:\t\t");
    scanf("%d\n",&n);
    
    bigint_t** T = create_bigint_table(n);
    
    read_command(T, n);
    
    free(T);

    return 0;
}
