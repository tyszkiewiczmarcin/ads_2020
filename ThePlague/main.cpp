#include <iostream>
#include <limits.h>
#include <string>
#include <vector>
#define min(a,b) a<b ? a : b
#define TRAVERSED(y) (P[y] != -1)
#define SOURCE 0
#define SINK (vertexCount-1)
using namespace std;

struct slistEl
{
    slistEl * next;
    int data;
};

class queue
{
private:
    slistEl * head;
    slistEl * tail;

public:
    queue();
    ~queue();
    const bool empty();
    const int  front();
    void push(int v);
    void pop();
};

queue::queue()
{
  head = tail = NULL;
}

queue::~queue()
{
    while(head)
        pop();
}

const bool queue::empty()
{
    return !head;
}

const int queue::front()
{
    if(head) return head->data;
    else     return -INT_MAX;
}

void queue::push(int v)
{
    slistEl * p = new slistEl;
    p->next = NULL;
    p->data = v;
    if(tail) tail->next = p;
    else     head = p;
    tail = p;
}

void queue::pop()
{
    if(head)
    {
        slistEl * p = head;
        head = head->next;
        if(!head) tail = NULL;
        delete p;
    }
}

class Person
{
public:
    string name;
    int value;
    int hTotal;
    int hDaily;
    string cathedralName;
    
    void scanInput()
    {
        cin >> name >> value >> hTotal >> hDaily;
    }
};

class Cathedral {
public:
    string   name;
    int      emplCount;
    Person** empl;
    
    void scanInput()
    {
        cin >> name >> emplCount;
        
        empl = new Person*[emplCount];
        for (int i = 0; i < emplCount; i++)
        {
            Person* p = new Person;
            p->scanInput();
            p->cathedralName = name;
            empl[i] = p;
        }
    }
};

class Task {
public:
    int day;
    int time;
    int hDemanded;
    int hMax;
    
    void scanInput()
    {
        cin >> day >> time >> hDemanded >> hMax;
    }
};

class Group {
public:
    string name;
    int    taskCount;
    int    hDemanded;
    int    hExtra;
    Task** task;
    
    void scanInput()
    {
        cin >> name >> taskCount >> hDemanded;
        
        hExtra = hDemanded;
        
        task = new Task*[taskCount];
        for (int i = 0; i < taskCount; i++)
        {
            Task* t = new Task;
            t->scanInput();
            task[i] = t;
            hExtra -= t->hDemanded;
        }
    }
};

class Crew
{
public:
    vector<Person> members;
    int value;
    
    Crew(vector<Person> c, int val)
    {
        members = c;
        value = val;
    }
    Crew()
    {
        value = 0;
    }
    void print()
    {
        cout << value << endl;
            
        int memberCount = (int)members.size();
        int cnt = 1;
        Person* first = &members[0];
        int i = 0;

        while (i++ < memberCount)
        {
            Person* next = &members[i];

            if (first->cathedralName == next->cathedralName)
                cnt += 1;
            else
            {
                cout << first->cathedralName << " " << cnt << endl;
                cnt = 1;
            }
            first = next;
        }
    }
};

class Graph
{
public:
    int vertexCount, edgeCount;
    int ** C;  // Capacity matrix
    int ** F;  // Flow matrix
    int * P;   // Parent array
    int * CFP; // Residual flow array
    
    Graph(int v)
    {
        vertexCount = v;
        edgeCount = 0;
        
        C = new int*[v];
        F = new int*[v];
        for(int i = 0; i < v; i++ )
        {
          C[i] = new int[v];
          F[i] = new int[v];
        }
        for(int i = 0; i < v; i++ )
            for(int j = 0; j < v; j++ )
                C[i][j] = F[i][j] = 0;
        
        P = new int[v];
        CFP = new int[v];
    }
    
    void addEdge(int x, int y, int cp)
    {
        C[x][y] = cp;
        edgeCount++;
    }
    int EdmondsKarpMaximumFlow();
};

class Plague {
public:
    int           mode;
    int           outputOpt;
    Cathedral**   C;
    Task**        T;
    Group**       G;
    int           emplCount, dayCount, timeCount, allTaskCount, groupCount, cathedralCount, treeNodeCount;
    int           vertexCount;
    
    int           maxEmplInCrew;

    vector<int>   days;
    vector<int>   times;
    
    vector<Crew*> possibleCrews;
    
    void  scanInput();
    void  getUniqueDaysNTimes();
    Graph createGraph(vector<Person> empl);
    bool  conditionsMet(Graph g);
    void  getCrews();
    void  getCrewsMode1(int place[], int level);
    void  getCrewsMode2(int place[], int level);
    void  sortCrews();
    Crew* findBestCrew();
    void  getTasks()
    {
        Task** t = new Task*[allTaskCount];
        int x = 0;
        for (int i = 0; i < groupCount; i++)
        {
            for (int j = 0; j < G[i]->taskCount; j++)
                t[x++] = G[i]->task[j];
        }
        T = t;
    }
};

bool unique(vector<int> vector, int num)
{
    for (int i = 0; i < (int)vector.size(); i++)
        if (vector[i] == num)
            return false;
    return true;
}

void Plague::scanInput()
{
    cin >> mode;
    cin >> outputOpt;
    
    int i, j;
    cin >> cathedralCount;
    C = new Cathedral*[cathedralCount];
    for (i = 0; i < cathedralCount; i++)
    {
        Cathedral* c = new Cathedral;
        c->scanInput();
        C[i] = c;
    }
    
    cin >> groupCount;
    allTaskCount = 0;
    G = new Group*[groupCount];
    for (i = 0; i < groupCount; i++)
    {
        Group* g = new Group;
        g->scanInput();
        G[i] = g;
        allTaskCount += g->taskCount;
    }
    
    getTasks();
    
    for (i = 0; i < groupCount; i++)
    {
        for (j = 0; j < G[i]->taskCount; j++)
        {
            // Add only unique days
            if (unique(days, G[i]->task[j]->day))
                days.push_back(G[i]->task[j]->day);
            // Add only unique times
            if (unique(times, G[i]->task[j]->time))
                times.push_back(G[i]->task[j]->time);
        }
    }
    dayCount  = (int)days.size();
    timeCount = (int)times.size();
}

Graph Plague::createGraph(vector<Person> crew)
{
    emplCount = (int)crew.size();
    
    treeNodeCount = 1 + emplCount + emplCount * dayCount + emplCount * dayCount * timeCount;
    
    vertexCount = emplCount
                + emplCount * dayCount
                + emplCount * dayCount * timeCount
                + allTaskCount + groupCount + 2;
    
    Graph g(vertexCount);
    
    int childIndex, parentIndex;
    for (int e = 0; e < emplCount; e++)
    {
        // S ===> E
        parentIndex = SOURCE;
        childIndex  = 1 + e;
        //cout << parentIndex << " " << childIndex << " " << empl[e]->hTotal << endl;
        g.addEdge(parentIndex, childIndex, crew[e].hTotal);
        
        for (int d = 0; d < dayCount; d++)
        {
            // E ===> E/D
            parentIndex = 1 + e;
            childIndex  = 1 + emplCount
                        + e * dayCount
                        + d;
            //cout << parentIndex << " " << childIndex << " " << empl[e]->hDaily << endl;
            g.addEdge(parentIndex, childIndex, crew[e].hDaily);
            
            for (int ti = 0; ti < timeCount; ti++)
            {
                // E/D ===> E/D/T
                parentIndex = 1 + emplCount
                            + e * dayCount
                            + d;
                childIndex  = 1 + emplCount + emplCount * dayCount
                            + e * dayCount * timeCount
                            + d * timeCount
                            + ti;
                //cout << parentIndex << " " << childIndex << " " << 1 << endl;
                g.addEdge(parentIndex, childIndex, 1);
                
                // E/D/T ===> TASK
                parentIndex = childIndex;
                for (int ta = 0; ta < allTaskCount; ta++)
                {
                    Task* compared = T[ta];
                    
                    if (compared->day == days[d] && compared->time == times[ti])
                    {
                        
                        childIndex  = treeNodeCount + ta;
                        g.addEdge(parentIndex, childIndex, 1);
                        //cout << parentIndex << " " << childIndex << " " << 1 << endl;
                    }
                }
            }
        }
    }
    days.clear();
    times.clear();
    
    for (int gr = 0; gr < groupCount; gr++)
    {
        int taskIndex = treeNodeCount;
        if (gr > 0)
            taskIndex += G[gr-1]->taskCount;
        for (int ta = 0; ta < G[gr]->taskCount; ta++)
        {
            parentIndex = taskIndex + ta;
            Task* task = G[gr]->task[ta];
            
            // TASK ===> GROUP
            childIndex = treeNodeCount + allTaskCount + gr;
            g.addEdge(parentIndex, childIndex, task->hMax - task->hDemanded);
            
            // TASK ===> SINK
            childIndex = SINK;
            g.addEdge(parentIndex, childIndex, task->hDemanded);
        }
        // GROUP ===> SINK
        parentIndex = treeNodeCount + allTaskCount + gr;
        childIndex  = SINK;
        g.addEdge(parentIndex, childIndex, G[gr]->hExtra);
    }
    return g;
}

int Graph::EdmondsKarpMaximumFlow()
{
    queue Q;
    int cp, x, y, i;
    bool esc;
    int total_flow = 0;

    while(true)
    {
        esc = false;            // Bool for loop escaping
        
        memset(P, -1, vertexCount * sizeof(int));

        P [SOURCE]   = -2;      // Source does not have a parent
        CFP [SOURCE] = INT_MAX; // The flow is initially very large

        while(!Q.empty())
            Q.pop();
        Q.push(SOURCE);
        
        while(!Q.empty()) // Search for augmenting path using BFS
        {
            x = Q.front();
            Q.pop( );

            for(y = 0; y < vertexCount; y++)
            {
                cp = C[x][y] - F[x][y];

                if(cp && !TRAVERSED(y)) // Check if edge exists and if was not traversed in the previous path
                {
                    P[y] = x; // X is a parent of Y

                    // Y residual capacity = MIN((current capacity),( flow X -> Y ))
                    CFP[y] = min(cp, CFP[x]);

                    // Path was found - update flow
                    if(y == SINK)
                    {
                        i = y;
                        while(i != SOURCE)
                        {
                            x = P[i];
                            F [x][i] += CFP [SINK];
                            F [i][x] -= CFP [SINK];
                            i = x;
                        }
                        total_flow += CFP [SINK];
                        esc = true; break;
                    }
                    // Path not found - push current edge to the queue
                    //                  and find other path
                    else
                        Q.push (y);
                }
            }
            // Possible path was found  - repeat the process of path finding
            if(esc) break;
          }
          // Possible path wasn't found - exit
          if(!esc) break;
    }
    return total_flow;
}

bool Plague::conditionsMet(Graph g)
{
    int i = treeNodeCount;
    while (i++ < vertexCount - 1)
    {
        if (g.C[i][SINK] - g.F[i][SINK] != 0)
            return false;
    }
    return true;
}

void Plague::getCrews()
{
    int* place = new int[cathedralCount];
    memset(place, -1, cathedralCount * sizeof(int));
    maxEmplInCrew = 0;
    getCrewsMode2(place, 0);
}

void Plague::getCrewsMode1(int place[], int level)
{
    for (int i = -1; i < C[level]->emplCount; i++)
    {
        place[level] = i;
        
        if (level == cathedralCount - 1)
        {
            int value = 0;
            vector<Person> members;
            for (int j = 0; j < cathedralCount; j++)
            {
                for (int k = 0; k < place[j] + 1; k++)
                {
                    members.push_back(*C[j]->empl[k]);
                    value += C[j]->empl[k]->value;
                }
            }
            possibleCrews.push_back(new Crew(members, value));
            members.clear();
        }
        else
        {
            getCrewsMode1(place, level + 1);
        }
    }
}
void Plague::getCrewsMode2(int place[], int level)
{
    for (int i = -1; i < C[level]->emplCount; i++)
    {
        place[level] = i;
        
        if (level == cathedralCount - 1)
        {
            int value = 0;
            vector<Person> members;
            for (int j = 0; j < cathedralCount; j++)
            {
                for (int k = 40; k < maxEmplInCrew; k++)
                {
                    members.push_back(*C[j]->empl[k]);
                    value += C[j]->empl[k]->value;
                }
            }
            possibleCrews.push_back(new Crew(members, value));
            members.clear();
        }
        else
        {
            getCrewsMode1(place, level + 1);
        }
        if (level == 0)
        {
            maxEmplInCrew = place[0];
        }
    }
}
void Plague::sortCrews()
{
    Crew* smallest = NULL;
    int smallestId;
    for (int i = 0; i < (int)possibleCrews.size(); i++)
    {
        smallest = possibleCrews[i];
        smallestId = INT_MAX;
        for (int j = i; j < (int)possibleCrews.size(); j++)
        {
            if (possibleCrews[j]->value < smallest->value)
            {
                smallest = possibleCrews[j];
                smallestId = j;
            }
        }
        if (smallestId != INT_MAX)
            swap(possibleCrews[i], possibleCrews[smallestId]);
    }
}

Crew* Plague::findBestCrew()
{
    for (int i = 0; i < (int)possibleCrews.size(); i++)
    {
        //cout << possibleCrews[i]->value << endl;
        Graph g = createGraph(possibleCrews[i]->members);

        int flow = g.EdmondsKarpMaximumFlow();
        
        if (conditionsMet(g))
        {
            cout << flow << endl;
            return possibleCrews[i];
        }
    }
    return NULL;
}

void scanTest()
{
    int testNum;
    cout << "Test number: "; cin >> testNum;
    string PATH = "input/test" + to_string(testNum);
    freopen(PATH.c_str(), "r", stdin);
}
/*
int main()
{
    scanTest();
    //freopen("input/test2", "r", stdin);
    
    Plague* p = new Plague;
    p->scanInput();
    p->getCrews();
    p->sortCrews();
    
    Crew* Best = p->findBestCrew();
    
    if (Best != NULL)
    {
        printf("r: %d, T: %d, d: %d, t: %d\n",p->emplCount, p->allTaskCount, p->dayCount, p->timeCount);
        Best->print();
    }
    else
        cout << "NO\n";
    delete p;
    return 0;
}
*/
int main()
{
    freopen("input/test4", "r", stdin);
    
    Plague* p = new Plague;
    p->scanInput();
    
    p->getCrews();
    int* r = new int[p->possibleCrews.size()];
    int* T = new int[p->possibleCrews.size()];
    int* d = new int[p->possibleCrews.size()];
    int* t = new int[p->possibleCrews.size()];
    int* f = new int[p->possibleCrews.size()];
    
    for (int i=0; i<p->possibleCrews.size(); i++)
    {
        Graph g = p->createGraph(p->possibleCrews[i]->members);
        r[i] = p->possibleCrews[i]->members.size();
        T[i] = p->allTaskCount;
        d[i] = p->dayCount;
        t[i] = p->timeCount;
        f[i] = g.EdmondsKarpMaximumFlow();
    }
    cout << "[";
    for (int i=0; i<p->possibleCrews.size(); i++)
    {
        cout << r[i] << ",";
    }
    cout << "]\n";
    cout << "[";
    for (int i=0; i<p->possibleCrews.size(); i++)
    {
        cout << T[i] << ",";
    }
    cout << "]\n";
    cout << "[";
    for (int i=0; i<p->possibleCrews.size(); i++)
    {
        cout << d[i] << ",";
    }
    cout << "]\n";
    cout << "[";
    for (int i=0; i<p->possibleCrews.size(); i++)
    {
        cout << t[i] << ",";
    }
    cout << "]\n";
    cout << "[";
    for (int i=0; i<p->possibleCrews.size(); i++)
    {
        cout << f[i] << ",";
    }
    cout << "]\n";
    delete p;
    return 0;
}
