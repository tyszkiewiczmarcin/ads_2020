## These are my school projects from Algorithms and Data Structures course
1) Bigint - Program capable of performing basic math operations (+, -, min, max) on integers of any size, for e.g. ![formula](https://render.githubusercontent.com/render/math?math=10^10000000).  
2) Enigma - Implementation of an [Enigma machine](https://en.wikipedia.org/wiki/Enigma_machine) variant.  
3) MAST - [Maximum Agreement SubTree](https://en.wikipedia.org/wiki/Maximum_agreement_subtree_problem) problem implementation. (Input: set of n rooted trees in the [NEWICK](https://en.wikipedia.org/wiki/Newick_format) format).  
4) The Plague - [Maximum flow problem](https://en.wikipedia.org/wiki/Maximum_flow_problem) implementation. Aim is to find the least expensive group of people capable of doing selected exercises over a certain time.  

Languages: C, C++
