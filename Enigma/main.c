#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define DEB false

typedef struct {
    int*         definition;
} permut_t;

typedef struct {
    int          size;
    permut_t*    permutation;
    permut_t*    permutation_inv;
    int          position;
    int*         notches;
    int          notch_count;
} rotor_t;

typedef struct {
    int          alphabet_size;
    int          rotor_count;
    rotor_t**    rotors;
    int          reflector_count;
    int**        reflectors;
} data_t;

typedef struct {
    int          alphabet_size;
    int          rotor_count;
    rotor_t**    rotors;
    int*         reflector;
    bool         first_moved;
    bool         second_moved;
} setting_t;

typedef enum {
    FORWARD,
    BACKWARD
} direction_t;

permut_t* create_inverse_permutation(permut_t* P, int size)
{
    int* definition = (int*)malloc(size * sizeof(int));
    for (int i = 0; i < size; i++)
    {
        definition[P->definition[i]] = i;
    }
    permut_t* I = (permut_t*)malloc(size * sizeof(permut_t));
    I->definition = definition;
    return I;
}

rotor_t* init_rotor(data_t* data)
{
    int notch_count, notch_definition, size = data->alphabet_size;
    
    int* definition = (int*)malloc(size * sizeof(int));
    for (int i = 0; i < size; i++)
    {
        scanf(" %d", &definition[i]);
        definition[i] -= 1;
    }
    
    scanf(" %d", &notch_count);
    int* notches = (int*)malloc(notch_count * sizeof(int));
    for (int i = 0; i < notch_count; i++)
    {
        scanf(" %d",&notch_definition);
        notch_definition -= 2;
        notch_definition += size;
        notch_definition %= size;
        notches[i] = notch_definition;
    }
    
    permut_t* permutation = (permut_t*)malloc(sizeof(permut_t));
    permutation->definition = definition;
    permut_t* permutation_inv = create_inverse_permutation(permutation, size);
    
    rotor_t* rotor = (rotor_t*)malloc(sizeof(rotor_t));
    rotor->size = size;
    rotor->permutation = permutation;
    rotor->permutation_inv = permutation_inv;
    rotor->position = 0;
    rotor->notches = notches;
    rotor->notch_count = notch_count;
    
    return rotor;
}

int* scan_reflector(data_t* data)
{
    int* reflector_definition = (int*)malloc(data->alphabet_size * sizeof(int));
    int number;
    for (int i = 0; i < data->alphabet_size; i++)
    {
        scanf(" %d", &number);
        number -= 1;
        reflector_definition[i] = number;
    }
    return reflector_definition;
}

data_t* input_machine_parts()
{
    data_t* data = (data_t*)malloc(sizeof(data_t));
    scanf(" %d", &data->alphabet_size);

    scanf(" %d", &data->rotor_count);
    rotor_t** ROTOR_ARR = (rotor_t**)malloc(data->rotor_count * sizeof(rotor_t*));
    for (int i = 0; i < data->rotor_count; i++)
        ROTOR_ARR[i] = init_rotor(data);

    scanf(" %d", &data->reflector_count);
    int** REFLECTOR_ARR = (int**)malloc(data->reflector_count * sizeof(int*));
    for (int i = 0; i < data->reflector_count; i++)
        REFLECTOR_ARR[i] = scan_reflector(data);

    data->reflectors = REFLECTOR_ARR;
    data->rotors = ROTOR_ARR;

    return data;
}

setting_t* input_settings(data_t* data)
{
    int rotor_count;
    scanf(" %d", &rotor_count);

    rotor_t** ROTOR_ARR = (rotor_t**)malloc(rotor_count * sizeof(rotor_t*));
    int copy_id = 0, ref_id = 0, rotor_init_pos = 0;
    
    
    for (int i = 0; i < rotor_count; i++)
    {
        scanf(" %d %d", &copy_id, &rotor_init_pos);
        
        rotor_init_pos += data->alphabet_size;
        rotor_init_pos -= 1;
        rotor_init_pos %= data->alphabet_size;
        
        permut_t* perm = (permut_t*)malloc(sizeof(permut_t));
        permut_t* perm_inv = (permut_t*)malloc(sizeof(permut_t));
        
        int* def = (int*)malloc(data->alphabet_size * sizeof(int));
        int* def_inv = (int*)malloc(data->alphabet_size * sizeof(int));
            
        for (int j = 0; j < data->alphabet_size; j++)
        {
            def[j] = data->rotors[copy_id]->permutation->definition[j];
            def_inv[j] = data->rotors[copy_id]->permutation_inv->definition[j];
        }
            
        perm->definition = def;
        perm_inv->definition = def_inv;
        
        int notch_count = data->rotors[copy_id]->notch_count;
        int* notches = (int*)malloc(notch_count * sizeof(int));
        
        for (int j=0; j<notch_count; j++)
            notches[j] = data->rotors[copy_id]->notches[j];
        
        rotor_t* rotor = (rotor_t*)malloc(sizeof(rotor_t));
        rotor->size = data->alphabet_size;
        rotor->permutation = perm;
        rotor->permutation_inv = perm_inv;
        rotor->position = rotor_init_pos;
        rotor->notches = notches;
        rotor->notch_count = notch_count;

        ROTOR_ARR[i] = rotor;
    }


    scanf(" %d", &ref_id);
    int* REFLECTOR = (int*)malloc(data->alphabet_size * sizeof(int));
    for (int i = 0; i < data->alphabet_size; i++)
        REFLECTOR[i] = data->reflectors[ref_id][i];
    
    setting_t* SETTING = (setting_t*)malloc(sizeof(setting_t));
    SETTING->alphabet_size = data->alphabet_size;
    SETTING->rotor_count = rotor_count;
    SETTING->rotors = ROTOR_ARR;
    SETTING->reflector = REFLECTOR;
    SETTING->first_moved = false;
    SETTING->second_moved = false;
    
    return SETTING;
}

int permutation_apply(permut_t* p, int number)
{
    return p->definition[number];
}


int evaluate_new_position(rotor_t* R, int number, direction_t d)
{
    number += R->position;
    number %= R->size;
    number  = permutation_apply(d == FORWARD ? R->permutation
                                             : R->permutation_inv, number);
    number += R->size;
    number -= R->position;
    number %= R->size;
    
    return number;
}

int encode_number(setting_t* S, int number)
{
    for (int i = 0; i < S->rotor_count; i++)
        number = evaluate_new_position(S->rotors[i], number, FORWARD);
    
    number = S->reflector[number];
    
    for (int i = S->rotor_count - 1; i >= 0; i--)
        number = evaluate_new_position(S->rotors[i], number, BACKWARD);

    return number;
}

bool rotor_at_notch(rotor_t* rotor)
{
    for (int i = 0; i < rotor->notch_count; i++)
    {
        if (rotor->position == rotor->notches[i])
            return true;
    }
    return false;
}

void advance_rotor(setting_t* S, int i)
{
    if (i == 2)
    {
        S->rotors[2]->position += 1;
        S->rotors[2]->position %= S->alphabet_size;
        i -= 1;
    }
    
    if (i == 1)
    {
        S->rotors[1]->position += 1;
        S->rotors[1]->position %= S->alphabet_size;
        S->second_moved = true;
    }
    
    S->rotors[0]->position += 1;
    S->rotors[0]->position %= S->alphabet_size;
    S->first_moved = true;
}

// encodes line of numbers
void encode(setting_t* S)
{
    int number;
    while (true)
    {
        scanf(" %d", &number);
        number -= 1;
        if (number < 0)
            break;
        
        if (S->rotor_count >= 3 && rotor_at_notch(S->rotors[1]) && S->second_moved)
            advance_rotor(S, 2);
            
        else if (S->rotor_count >= 2 && rotor_at_notch(S->rotors[0]) && S->first_moved)
            advance_rotor(S, 1);
        
        else
            advance_rotor(S, 0);
        
        number = encode_number(S, number);
        number += 1;
        printf("%d ",number);
    }
    printf("\n");
}

int main()
{
    data_t* data = input_machine_parts();

    int task_count;
    scanf(" %d", &task_count);
    printf("\n");
    
    for (int task = 0; task < task_count; task++)
    {
        setting_t* setting = input_settings(data);
        encode(setting);
        free(setting);
    }
    free(data);

    return 0;
}
