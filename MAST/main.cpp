#include <iostream>
#define ZERO true
#define ISLEAF(N) (N) > 0
#define ISNODE(N) (N) <= 0
#define TRAVERSED(X, Y, Z) (X)->traversed[abs(Y)][abs(Z)]

typedef struct node {
    int          value;
    int          child_count;
    struct node* parent;
    struct node* sibling;
    struct node* child;
} node_t;

typedef struct lnode {
    node_t       *node_ptr;
    struct lnode *next;
} list_t;

typedef struct {
    node_t  *root;
    int      node_count;
    int      leaf_count;
    node_t** leaf_arr;
    node_t** node_arr;
} tree_t;

typedef struct {
    int     max;
    int**   hashmap;
    bool**  traversed;
    tree_t* t1;
    tree_t* t2;
} mast_ctx_t;

typedef struct {
    int*   arr;
    bool*  banned_cols;
    int    max_sum;
} data_t;

node_t* init_node() {
    node_t* node       = (node_t*)malloc(sizeof(node_t));
    node->value        = 0;
    node->child_count  = 0;
    node->parent       = NULL;
    node->sibling      = NULL;
    node->child        = NULL;
    return node;
}

list_t* init_list() {
    list_t* node       = (list_t*)malloc(sizeof(list_t));
    node->node_ptr     = NULL;
    node->next         = NULL;
    return node;
}

tree_t* init_tree() {
    tree_t* tree       = (tree_t*)malloc(sizeof(tree_t));
    tree->root         = NULL;
    tree->node_count   = 0;
    tree->leaf_count   = 0;
    return tree;
}

mast_ctx_t* init_ctx() {
    mast_ctx_t* ctx = (mast_ctx_t*)malloc(sizeof(mast_ctx_t));
    ctx->max          = 0;
    ctx->hashmap      = NULL;
    ctx->traversed    = NULL;
    ctx->t1           = NULL;
    ctx->t2           = NULL;
    return ctx;
}

data_t* init_data() {
    data_t* data      = (data_t*)malloc(sizeof(data_t));
    data->arr         = NULL;
    data->banned_cols = NULL;
    data->max_sum     = 0;
    return data;
}

void init_traversed(mast_ctx_t* ctx) {
    int len = ctx->t2->node_count;
    int h   = ctx->t1->node_count;
    int i, j;
    bool** traversed_array = (bool**)malloc(h * sizeof(bool*));
    for (i = 0; i < h; i++) {
        bool* traversed_row = (bool*)malloc(len * sizeof(bool));
        for (j = 0; j < len; j++) {
            traversed_row[j] = false;
        }
        traversed_array[i] = traversed_row;
    }
    ctx->traversed = traversed_array;
}
// - - - - - - - - - - - - - - - - - - // list functions

void push_back(list_t* head, node_t* ptr) {
    list_t* current = head;
    if (current->node_ptr == NULL) {
        current->node_ptr = ptr;
        return;
    }
    while (current->next != NULL)
        current = current->next;
    current->next = init_list();
    current->next->node_ptr = ptr;
}

// - - - - - - - - - - - - - - - - - - // tree functions

void print_hashmap(mast_ctx_t* ms) {
    int l, x, y, i, j;
    l = ms->t1->leaf_count;
    x = ms->t2->node_count;
    y = ms->t1->node_count;
    
    printf("    │");
    
    // print T2 leaves at horizontal axis
    for (i = 0; i < l; i++)
    {
        if (i + 1 < 10)
            printf(" ");
        printf(" %d",i + 1);
    }
    
    // print T2 node at horizontal axis
    for (i = 0; i < x; i++)
    {
        if (i < 10)
            printf(" ");
        printf("-%d",i);
    }
    printf("\n");
    
    // horizontal separator
    printf("────┼");
    for (i = 0; i < l + x; i++)
        printf("───");
    printf("\n");
    
    // print T1 leaves at vertical axis and hashmap
    for (i = 0; i < l; i++)
    {
        if (i + 1 < 10)
            printf(" ");
        printf(" %d │",i + 1);
        // print hashmap
        for (j = 0; j < l + x; j++)
        {
            int hashElement = ms->hashmap[i][j];
            if (hashElement == 0)
                printf("  ○");
            else
                printf("  %d",hashElement);
        }
        printf("\n");
    }
    
    // print T1 nodes at vertical axis and hashmap
    for (i = 0; i < y; i++)
    {
        if (y < 10)
            printf(" ");
        printf("-%d │",i);
        // print hashmap
        for (j = 0; j < l + x; j++)
        {
            int hashElement = ms->hashmap[i + l][j];
            if (j >= l && ms->traversed[i][j-l] == false)
                printf("  -");
            else if (hashElement == 0)
                printf("  ○");
            else
                printf("  %d",hashElement);
        }
        printf("\n");
    }
    printf("\n");
}

tree_t* scan_tree() {
    char c;
    int value;
    int node_count = 0;
    int leaf_count = 0;
    list_t* leaf_list = init_list();
    list_t* node_list = init_list();
    node_t* root      = init_node();
    node_t* current   = root;
    
    while (scanf("%c",&c) && c != ';') {
        // 1. assign node id
        // 2. add new child
        if (c == '(') {
            current->value = node_count * (-1);
            current->child_count = 1;
            push_back(node_list, current);
            node_count += 1;
            
            
            current->child = init_node();
            current->child->parent = current;
            current = current->child;
        }
        // add new brother
        else if (c == ',') {
            current->sibling = init_node();
            current->sibling->parent = current->parent;
            current->parent->child_count += 1;
            current = current->sibling;
        }
        // go back to parent
        else if (c == ')') {
            current = current->parent;
        }
        
        if (scanf("%d",&value)) {
            current->value = value;
            leaf_count += 1;
            push_back(leaf_list, current);
        }
    }
    
    node_t** leaf_arr = (node_t**)malloc(leaf_count * sizeof(node_t*));
    for (int i = 0; i < leaf_count; i++)
    {
        // create array of leaves in rising order to not remember the indexes
        leaf_arr[leaf_list->node_ptr->value - 1] = leaf_list->node_ptr;
        leaf_list = leaf_list->next;
    }
    node_t** node_arr = (node_t**)malloc(node_count * sizeof(node_t*));
    for (int i = 0; i < node_count; i++)
    {
        node_arr[i] = node_list->node_ptr;
        node_list   = node_list->next;
    }
    
    tree_t* tree     = init_tree();
    tree->root       = root;
    tree->node_count = node_count;
    tree->leaf_count = leaf_count;
    tree->leaf_arr   = leaf_arr;
    tree->node_arr   = node_arr;
    return tree;
}

// - - - - - - - - - - - - - - - - - - // hashmap functions

void init_hashmap(mast_ctx_t* ctx) {
    int length = ctx->t2->leaf_count + ctx->t2->node_count;
    int height = ctx->t1->leaf_count + ctx->t1->node_count;
    int y, x;
    int** arr = (int**)malloc(height * sizeof(int*));
    for (y = 0; y < height; y++) {
        int* row = (int*)malloc(length * sizeof(int));
        for (x = 0; x < length; x++)
            row[x] = 0;
        arr[y] = row;
    }
    ctx->hashmap = arr;
}

void find_max(data_t* data, int length, int last_level, int level_max, int last_sum)
{
    int x, sum;
    int level = last_level + 1;
    
    for (x = 0; x < length; x++)
    {
        if (data->banned_cols[x])
            continue;
        data->banned_cols[x] = true;
        
        sum = last_sum + data->arr[(level - 1) * length + x];
        
        if (level != level_max)
            find_max(data, length, level, level_max, sum);
        
        else if (sum > data->max_sum)
            data->max_sum = sum;

        data->banned_cols[x] = false;
    }
}

int auction_algorithm_brute_force(int** values, int len, int h) {
    int y, x;
    int length = len >= h ? len : h;
    int height = len <= h ? len : h;

    data_t* data      = init_data();
    data->banned_cols = (bool*)malloc(height * sizeof(bool));
    
    // resize the array if needed
    int* arr = (int*)malloc(height * length * sizeof(int));
    for (y = 0; y < height; y++)
    {
        for (x = 0; x < length; x++)
        {
            if (len < h)
                arr[y * length + x] = values[x][y];
            else
                arr[y * length + x] = values[y][x];
            if (y == 0)
                data->banned_cols[x] = false;
        }
    }
    data->arr = arr;
    
    int maxlevel = height;
    find_max(data, length, 0, maxlevel, 0);
    
    int result = data->max_sum;
    free(arr);
    free(data);
    return result;
}

int calculate_points(mast_ctx_t* ctx, int node1_value, int node2_value) {
    int i, j, v1, v2, tmp = 0, max = 0;
    int leaf_count = ctx->t1->leaf_count;
    
    node_t* n1 = ctx->t1->node_arr[node1_value];
    node_t* n2 = ctx->t2->node_arr[node2_value];
    
    node_t* c1 = NULL;
    node_t* c2 = NULL;
    
    // III - associate sons of both nodes
    int** arr = (int**)malloc(n1->child_count * sizeof(int*));
    c1 = n1->child;
    for (i = 0; i < n1->child_count; i++)
    {
        v1 = c1->value;
        c2 = n2->child;
        int* row = (int*)malloc(n2->child_count * sizeof(int));
        for (j = 0; j < n2->child_count; j++)
        {
            v2 = c2->value;
            if      ( ISLEAF(v1) && ISLEAF(v2) )
                row[j] = v1 == v2 ? 1 : 0;
            
            else if ( ISLEAF(v1) && ISNODE(v2) )
                row[j] = ctx->hashmap[v1 - 1][leaf_count + abs(v2)];
            
            else if ( ISNODE(v1) && ISLEAF(v2) )
                row[j] = ctx->hashmap[leaf_count + abs(v1)][v2 - 1];
            
            else
            {
                 if ( TRAVERSED(ctx, v1, v2) )
                    row[j] = ctx->hashmap[leaf_count + abs(v1)][leaf_count + abs(v2)];
                else
                    row[j] = calculate_points(ctx, abs(v1), abs(v2));
            }
            c2 = c2->sibling;
        }
        c1 = c1->sibling;
        arr[i] = row;
    }
    tmp = auction_algorithm_brute_force(arr, n2->child_count, n1->child_count);
    if (tmp > max)
        max = tmp;
    free(arr);
    
    // I - identify sons of node 1 with node 2
    c1 = n1->child;
    for (i = 0; i < n1->child_count; i++)
    {
        v1 = c1->value;
        if (ISLEAF(v1))
            tmp = ctx->hashmap[abs(v1)    - 1      ][leaf_count + node2_value];
        else
            tmp = ctx->hashmap[leaf_count + abs(v1)][leaf_count + node2_value];

        if (tmp > max)
            max = tmp;
        
        c1 = c1->sibling;
    }
    
    // II - identify sons of node 2 with node 1
    c2 = n2->child;
    for (i = 0; i < n2->child_count; i++)
    {
        v2 = c2->value;
        if (ISLEAF(v2))
            tmp = ctx->hashmap[leaf_count + node1_value][abs(v2)    - 1];
        else
            tmp = ctx->hashmap[leaf_count + node1_value][leaf_count + abs(v2)];

        if (tmp > max)
            max = tmp;
    }

    ctx->hashmap[leaf_count + node1_value][leaf_count + node2_value] = max;
    if (max > ctx->max)
        ctx->max = max;
    ctx->traversed[node1_value][node2_value] = true;
    
    return max;
}

void fill_hashmap(mast_ctx_t* ctx) {
    int i, j;
    node_t* current = NULL;
    int leaf_count = ctx->t1->leaf_count;
    
    for (i = 0; i < leaf_count; i++)
    {
        // T1 leaf x T2 leaf
        for (j = 0; j < leaf_count; j++)
            ctx->hashmap[j][i] = i == j ? 1 : 0;
        
        // T1 leaf x T2 node
        current = ctx->t2->leaf_arr[i];
        while (current->parent != NULL)
        {
            current = current->parent;
            ctx->hashmap[i][leaf_count + abs(current->value)] = 1;
        }
        
        // T1 node x T2 leaf
        current = ctx->t1->leaf_arr[i];
        while (current->parent != NULL)
        {
            current = current->parent;
            ctx->hashmap[leaf_count + abs(current->value)][i] = 1;
        }
    }
    for (i = ctx->t1->node_count - 1; i >= 0; i--)
    {
        for (j = ctx->t2->node_count - 1; j >= 0; j--)
        {
            if (TRAVERSED(ctx, i, j))
                continue;
            calculate_points(ctx, i, j);
        }
    }
    
    ctx->hashmap[leaf_count][leaf_count] = ctx->max;
    printf("%d\n",leaf_count - ctx->max);
}

int main() {
    freopen("/Users/marcin/Desktop/School/2\ Sem/Algorithms\ and\ data\ structures/PROJECTS/Tree/Tree/inputs/stos_example", "r", stdin);
    int tree_buf_size, i, j;
    scanf(" %d",&tree_buf_size);
    
    tree_t** tree_buf = (tree_t**)malloc(tree_buf_size * sizeof(tree_t*));
    for (i = 0; i < tree_buf_size; i++)
        tree_buf[i] = scan_tree();
    
    for (i = 0; i < tree_buf_size; i++)
    {
        for (j = i + 1; j < tree_buf_size; j++)
        {
            mast_ctx_t* ctx = init_ctx();
            ctx->t1 = tree_buf[i];
            ctx->t2 = tree_buf[j];
            init_hashmap(ctx);
            init_traversed(ctx);
            fill_hashmap(ctx);
            print_hashmap(ctx);
            free(ctx);
        }
    }

    free(tree_buf);
    return 0;
}


